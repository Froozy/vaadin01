package com.example.vaadindemo;

import com.vaadin.annotations.Title;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;

	@Override
	protected void init(VaadinRequest request) {

		VerticalLayout vl = new VerticalLayout();
		HorizontalLayout hl = new HorizontalLayout();
		VerticalLayout vl2 = new VerticalLayout();
		
		
		final Label wynik= new Label("Wynik");
		final TextField TF1 = new TextField();
		final TextField TF2 = new TextField();
		
		Button plus = new Button("+");
		Button minus = new Button("-");
		Button mnozenie = new Button("*");
		Button dzielenie = new Button("/");
		
		vl.addComponent(TF1);
		hl.addComponent(plus);
		hl.addComponent(minus);
		hl.addComponent(mnozenie);
		hl.addComponent(dzielenie);
		vl.addComponent(hl);
		vl.addComponent(TF2);
		vl.addComponent(wynik);
		
		
		
		setContent(vl);

		plus.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				int x = Integer.parseInt(TF1.getValue());
				int y = Integer.parseInt(TF2.getValue());
				x = x+y;
				wynik.setValue(Integer.toString(x));
			}
		});
minus.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				int x = Integer.parseInt(TF1.getValue());
				int y = Integer.parseInt(TF2.getValue());
				x = x-y;
				wynik.setValue(Integer.toString(x));
			}
		});
mnozenie.addClickListener(new ClickListener() {
	
	@Override
	public void buttonClick(ClickEvent event) {
		
		int x = Integer.parseInt(TF1.getValue());
		int y = Integer.parseInt(TF2.getValue());
		x = x*y;
		wynik.setValue(Integer.toString(x));
	}
});
dzielenie.addClickListener(new ClickListener() {
	
	@Override
	public void buttonClick(ClickEvent event) {
		
		int x = Integer.parseInt(TF1.getValue());
		int y = Integer.parseInt(TF2.getValue());
		x = x/y;
		wynik.setValue(Integer.toString(x));
	}
});	
	}
}
